# Feature Extraction

This is the feature extraction exercise for FL8 week 4 day 2 by Gilad Rosenblatt.

## Instructions

### Run

Run `stitching.py`/`blending.py` in `code` as working directory to stitch/blend two saved images, respectively.

### Data

The two images for stitching are `left.jpeg` and `right.jpeg`, and these files should be in an `images` folder. The two images for blending are `black.jpeg` and `white.jpeg` (blended according to the mask image `mask.png`), and these files should be in an `images` folder.

## Results

### Stitching

Below are the keypoints found for each image (green circles) and the matches found between them (colored lines).

![keypoints](/images/keypoints.png?raw=true).

Stitching the two images using a holography calculated from the above keypoint matches yields the image below.

![stitching](/images/stitching.png?raw=true).

Stitching the same images, but adding blending to smoothen the transition yields the image below.

![stitching_blending](/images/stitching_with_blending.png?raw=true).

### Blending

Below is the blending between the `black.jpeg` and `white.jpeg` images.

![blending](/images/blended_image.png?raw=true).

## License

[WTFPL](http://www.wtfpl.net/)
