import cv2
import numpy as np
from scipy.signal import convolve2d


def generate_kernel(parameter):
    """
    Return a 5x5 generating kernel based on an input parameter.

    :param float parameter: value in range [0, 1].
    :returns numpy.ndarray: a 5x5 kernel.
    """
    kernel_basis = np.array([0.25 - parameter / 2.0, 0.25, parameter, 0.25, 0.25 - parameter / 2.0])
    return np.outer(kernel_basis, kernel_basis)


def reduce(image):
    """
    Convolve the input image with a generating kernel of parameter of 0.4 and then reduce its width and height by two.

    :param numpy.ndarray image: a grayscale image of shape (r, c)
    :return numpy.ndarray: an image of shape (ceil(r/2), ceil(c/2)), e.g., if the input is 5x7, the output will be 3x4.
    """
    kernel = generate_kernel(parameter=0.4)
    convolved_image = convolve2d(image, kernel, mode="same")
    return convolved_image[::2, ::2].copy()


def expand(image):
    """
    Expand the image to double the size and then convolve it with a generating kernel with a parameter of 0.4.
    You should upsample the image, and then convolve it with a generating kernel of a = 0.4. Finally, multiply your
    output image by a factor of 4 in order to scale it back up. If you do not do this (and I recommend you try it out
    without that) you will see that your images darken as you apply the convolution (why?).

    :param numpy.ndarray image: a grayscale image of shape (r, c)
    :return numpy.ndarray: an image of shape (2*r, 2*c)
    """
    target_shape = tuple(2 * dimension for dimension in image.shape)
    upsampled_image = np.zeros(shape=target_shape, dtype=np.float64)
    upsampled_image[::2, ::2] = image
    kernel = generate_kernel(parameter=0.4)
    return 4 * convolve2d(upsampled_image, kernel, mode="same")


def build_gaussian_pyramid(image, levels):
    """
    Build a Gaussian pyramid from the image by reducing it by the number of levels passed as input.

    :param numpy.ndarray image: a grayscale image of dimension (r,c) and type float.
    :param uint8 levels: a positive integer that specifies the number of reductions, len(output) = levels + 1.
    :returns list: a list of numpy.ndarray arrays of type np.float, such that the first element is layer 0 of the
    Gaussian pyramid (the image itself), the second element is layer 1 (image reduced once), etc.
    """
    output = [image]
    for _ in range(levels):
        output.append(reduce(output[-1]))
    return output


def build_laplacian_pyramid(gaussian_pyramid):
    """
    Build a laplacian pyramid from a Gaussian pyramid of height levels.

    :param list gaussian_pyramid: a Gaussian pyramid as a list of numpy.ndarray arrays of type np.float.
    :returns list: a Laplacian pyramid of the same size as the input gaussian_pyramid, represented as a list of
    numpy.arrays of type np.float. Every element of the list is the difference between two layers of the Gaussian
    pyramid: output[k] = gaussian_pyramid[k] - expand(gaussian_pyramid[k + 1]). The last element is identical
    to the last layer of the Gaussian pyramid since it cannot be subtracted anymore.

    NOTE: The size of the expanded image may be larger than the given layer. In that case, crop the expanded image to
    match the shape of the given layer. For example, if a layer is of size 5x7, reduce and expand results in an
    image of size 6x8. In this case, crop the expanded layer to 5x7 by removing the last row/column.
    """

    # Append last level as-is from Gaussian to Laplacian pyramid.
    output = [gaussian_pyramid[-1]]

    # Iteratively append differences between consecutive levels to Laplacian pyramid (with matched shapes).
    for this_image, previous_image in zip(reversed(gaussian_pyramid[:-1]), reversed(gaussian_pyramid)):
        output.append(this_image - expand(previous_image)[:this_image.shape[0], :this_image.shape[1]])

    # Reverse and return the Laplacian pyramid.
    return list(reversed(output))


def blend_pyramids(laplacian_pyramid_white, laplacian_pyramid_black, gaussian_pyramid_mask):
    """
    Blend the two laplacian pyramids by weighting them according to the gaussian mask pyramid. All pyramids have the
    same number of levels, and the array in such each layer has the same shape across all pyramids.

    :param list laplacian_pyramid_white: a laplacian pyramid of one image.
    :param list laplacian_pyramid_black: a laplacian pyramid of another image.
    :param list gaussian_pyramid_mask: A gaussian pyramid of the mask (values are in the range of [0, 1]).
    :return list: a laplacian pyramid of the same dimensions as the input pyramids, where every layer is an alpha
    blend of the corresponding layers of the input pyramids, weighted by the gaussian mask, as follows:
    output[i, j] = current_mask[i, j] * white_image[i, j] + (1 -  current_mask[i, j]) * black_image[i, j]
    Hence, pixels where current_mask == 1 are taken entirely from the white image whereas those where current_mask == 0
    are taken entirely from the black image (current_mask, white_image, black_image refer to current pyramid layer).
    """
    blended_pyramid = []
    for white, black, mask in zip(laplacian_pyramid_white, laplacian_pyramid_black, gaussian_pyramid_mask):
        blended_pyramid.append(white * mask + black * (1 - mask))
    return blended_pyramid


def collapse(pyramid):
    """
    Collapse an input pyramid.

    :param list pyramid: a list of images as numpy.ndarray arrays (e.g., output of build_laplacian_pyramid, blend).
    :returns numpy.ndarray: an image of the same shape as the base layer of the pyramid and type np.float.

    NOTE: The size of the expanded image may be larger than the given layer. In that case, crop the expanded image to
    match the shape of the given layer. For example, if a layer is of size 5x7, reduce and expand results in an
    image of size 6x8. In this case, crop the expanded layer to 5x7 by removing the last row/column.
    """
    collapsed_image = pyramid[-1]
    for this_level in reversed(pyramid[:-1]):
        collapsed_image = this_level + expand(collapsed_image)[:this_level.shape[0], :this_level.shape[1]]
    return collapsed_image


def blend_images(black, white, mask, levels=7):
    """
    Blend two images according to a mask using an alpha blend of their laplacian pyramids.

    :param black: first BGR image.
    :param white: second BGR image.
    :param mask: black/white mask third BGR image.
    :param int levels: number of levels to use in building pyramids.
    :return: blended BGR image according to black/white mask.
    """

    # Split images and mask to channels (and convert to float / binary).
    names = ["blue", "green", "red"]
    black_channels = {name: channel.astype(np.float64) for name, channel in zip(names, cv2.split(black))}
    white_channels = {name: channel.astype(np.float64) for name, channel in zip(names, cv2.split(white))}
    mask_channels = {name: channel.astype(np.float64) / 255 for name, channel in zip(names, cv2.split(mask))}

    # Create Laplacian pyramids for black image (per channel).
    black_pyrs = {}
    for name, channel in black_channels.items():
        black_pyrs[name] = build_laplacian_pyramid(build_gaussian_pyramid(channel, levels))

    # Create Laplacian pyramids for white image (per channel).
    white_pyrs = {}
    for name, channel in white_channels.items():
        white_pyrs[name] = build_laplacian_pyramid(build_gaussian_pyramid(channel, levels))

    # Create Gaussian pyramids for black image (per channel).
    mask_pyrs = {}
    for name, channel in mask_channels.items():
        mask_pyrs[name] = build_gaussian_pyramid(channel, levels)

    # Alpha blend pyramids by mask values (per channel).
    blended_pyrs = {}
    for name, b_pyr, w_pyr, m_pyr in zip(names, black_pyrs.values(), white_pyrs.values(), mask_pyrs.values()):
        blended_pyrs[name] = blend_pyramids(w_pyr, b_pyr, m_pyr)

    # Collapse blended pyramid into image channels.
    blended_channels = {}
    for name, pyr in blended_pyrs.items():
        blended_channels[name] = collapse(pyr)

    # Merge blended image channels into blended image (clip and convert back to 8-bit unsigned int).
    blended_image = cv2.merge((
        np.clip(blended_channels["blue"], 0, 255).astype(np.uint8),
        np.clip(blended_channels["green"], 0, 255).astype(np.uint8),
        np.clip(blended_channels["red"], 0, 255).astype(np.uint8)
    ))

    # Return the blended image.
    return blended_image


def main():
    # Set file location dictionary (change here if folder structure changes).
    filenames = dict(
        black="../images/black.jpg",
        white="../images/white.jpg",
        mask="../images/mask.jpg"
    )

    # Set number of levels for pyramids to lucky-number-7 just because.
    levels = 7

    # Load black/white/mask images from file and blend the black and white images according to the mask.
    blended_image = blend_images(**{key: cv2.imread(filename) for key, filename in filenames.items()}, levels=levels)

    # Plot and save the blended image.
    cv2.imshow("blended_image", blended_image)
    cv2.waitKey()
    cv2.destroyAllWindows()
    cv2.imwrite("../images/blended_image.png", blended_image)


if __name__ == "__main__":
    main()
