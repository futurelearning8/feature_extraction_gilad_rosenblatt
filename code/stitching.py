import cv2
import numpy as np
import matplotlib.pyplot as plt
from blending import blend_images


def main(display_with_matplotlib=False):

    # Set file location dictionary (change here if folder structure changes).
    filenames = dict(
        left="../images/left.jpeg",
        right="../images/right.jpeg"
    )

    # Load left and right images from file.
    image_left = cv2.imread(filenames["left"])
    image_right = cv2.imread(filenames["right"])

    # Convert to grayscale.
    image_left_gray = cv2.cvtColor(image_left, cv2.COLOR_BGR2GRAY)
    image_right_gray = cv2.cvtColor(image_right, cv2.COLOR_BGR2GRAY)

    # Detect keypoints in both images.
    detector = cv2.ORB_create()
    kp_left, descriptors_left = detector.detectAndCompute(image_left_gray, None)
    kp_right, descriptors_right = detector.detectAndCompute(image_right_gray, None)

    # Find possible matching between keypoints in both images.
    bf = cv2.BFMatcher()
    matches = bf.knnMatch(descriptors_left, descriptors_right, k=2)

    # Qualify matches by distance threshold.
    threshold = 0.75
    passing_matches = []
    for m, n in matches:
        if m.distance < threshold * n.distance:
            passing_matches.append([m])

    # Draw all keypoints and matches onto both images (side-by-side).
    image_joined = cv2.drawMatchesKnn(
        cv2.drawKeypoints(image_left, kp_left, outImage=None, color=(0, 255, 0), flags=0),  # Left image w/all kps.
        kp_left,
        cv2.drawKeypoints(image_right, kp_right, outImage=None, color=(0, 255, 0), flags=0),  # Right image w/all kps.
        kp_right,
        passing_matches,
        flags=2,
        outImg=None
    )

    # Save the side-by-side keypoint matches image to file.
    cv2.imwrite("../images/keypoints.png", image_joined)

    # Compute homography between matching keypoints (left --> right).
    src = np.float64([kp_left[match[0].queryIdx].pt for match in passing_matches]).reshape(-1, 1, 2)
    dst = np.float64([kp_right[match[0].trainIdx].pt for match in passing_matches]).reshape(-1, 1, 2)
    homography, _ = cv2.findHomography(src, dst, cv2.RANSAC, 5.0)

    # Wrap left image according to homography.
    h, w, d = image_left.shape
    image_left_wrapped = cv2.warpPerspective(image_left, homography, (w, h))

    # Mask right image according to homography
    black = np.array([0, 0, 0], dtype=np.int8)
    mask = cv2.inRange(image_left_wrapped, black, black)
    image_right_masked = cv2.bitwise_and(image_right, image_right, mask=mask)

    # Stitch left and right images after wrapping/masking.
    image_stitched = image_left_wrapped + image_right_masked

    # Blend left and right images after wrapping/masking.
    image_blended = blend_images(
        black=image_left_wrapped,
        white=image_right,
        mask=np.tile(mask[:, :, np.newaxis], [1, 1, 3]),
        levels=10  # Enough to get to 1 pixel at last level of pyramid.
    )

    # Save the resulting stitched image to file.
    cv2.imwrite("../images/stitching.png", image_stitched)
    cv2.imwrite("../images/stitching_with_blending.png", image_blended)

    # Show stitched image with matplotlib / opencv.
    if display_with_matplotlib:
        plt.imshow(cv2.cvtColor(image_stitched, cv2.COLOR_BGR2RGB))  # Convert BGR to RGB.
        plt.show()
    else:
        cv2.imshow("stitching", image_stitched)
        cv2.waitKey()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
